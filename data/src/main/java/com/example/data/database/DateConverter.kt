package com.example.data.database

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.TypeConverter
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DateConverter {

    @TypeConverter
    fun fromStrDateTime(strDate: String): LocalDateTime =
        LocalDateTime.parse(strDate)

    @TypeConverter
    fun dateToStrDateTIme(date: LocalDateTime): String =
        date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
}