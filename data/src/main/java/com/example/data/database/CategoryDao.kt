package com.example.data.database

import androidx.room.*
import com.example.data.entities.CategoryEntity
import com.example.data.entities.CategoryWithNumOfNote
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {
    @Insert
    suspend fun insertCategory(category: CategoryEntity)

    @Delete
    suspend fun deleteCategory(category: CategoryEntity)

    @Update
    suspend fun updateCategory(category: CategoryEntity)

    @Query("SELECT c.*, COUNT(note.noteId) as number_of_notes FROM category as c left outer join note ON c.categoryId = note.categoryId  GROUP BY c.categoryId")
    fun fetchAllCategories(): Flow<List<CategoryWithNumOfNote>>
}