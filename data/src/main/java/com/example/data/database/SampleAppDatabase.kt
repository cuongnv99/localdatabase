package com.example.data.database

import android.content.Context
import androidx.room.*
import androidx.room.migration.AutoMigrationSpec
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.data.entities.CategoryEntity
import com.example.data.entities.NoteEntity

@Database(
    entities = [NoteEntity::class, CategoryEntity::class],
    version = 5,
    exportSchema = true,
    autoMigrations = [
        AutoMigration(from = 1, to = 2),
        AutoMigration(2, 3),
        AutoMigration(3, 4, SampleAppDatabase.Migration3To4::class),
    ]
)
@TypeConverters(DateConverter::class)
abstract class SampleAppDatabase : RoomDatabase() {
    @RenameColumn(fromColumnName = "author", toColumnName = "authorNew", tableName = "category")
    class Migration5To6 : AutoMigrationSpec

    @RenameColumn(
        tableName = "note",
        fromColumnName = "priority",
        toColumnName = "categoryPriority"
    )
    class Migration3To4 : AutoMigrationSpec

    abstract fun noteDao(): NoteDao
    abstract fun categoryDao(): CategoryDao

    companion object {
        @Volatile
        private var INSTANCE: SampleAppDatabase? = null

        fun getDatabase(context: Context): SampleAppDatabase {
            return synchronized(this) {
                INSTANCE ?: kotlin.run {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        SampleAppDatabase::class.java,
                        "word_database"
                    ).addMigrations(MIGRATION_4_5).build()
                    INSTANCE = instance
                    instance
                }
            }
        }
    }
}

val MIGRATION_4_5 = object : Migration(4, 5) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.apply {
            execSQL("CREATE TABLE IF NOT EXISTS `_new_category` (`categoryId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `color` TEXT NOT NULL, `level` TEXT NOT NULL DEFAULT '0', `authorNew` TEXT NOT NULL DEFAULT '0')")
            execSQL("INSERT INTO _new_category(categoryId, name, color, level, authorNew) SELECT categoryId, name, color, level, authorNew FROM category")
            execSQL("DROP TABLE category")
            execSQL("ALTER TABLE _new_category RENAME TO category")
        }
    }
}