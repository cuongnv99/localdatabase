package com.example.data.database

import androidx.room.*
import com.example.data.entities.*
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(vararg note: NoteEntity)

    @Delete
    suspend fun deleteNote(vararg note: NoteEntity)

    @Update
    suspend fun updateNote(vararg note: NoteEntity)

    @Query("SELECT note.*, category.color as color FROM note INNER JOIN category ON note.categoryId = category.categoryId WHERE category.categoryId = :categoryId")
    fun findNotesByCategory(categoryId: Long): Flow<List<NoteWithCategoryColor>>
}