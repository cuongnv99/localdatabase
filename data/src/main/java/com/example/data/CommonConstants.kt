package com.example.data

object CommonConstants {
    const val NOTE_TABLE_NAME = "note"
    const val CATEGORY_TABLE_NAME = "category"
    const val PRIORITY_TABLE_NAME = "priority"
}