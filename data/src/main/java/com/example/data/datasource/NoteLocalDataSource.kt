package com.example.data.datasource

import com.example.data.database.CategoryDao
import com.example.data.database.NoteDao
import com.example.data.entities.CategoryEntity
import com.example.data.entities.CategoryWithNumOfNote
import com.example.data.entities.NoteEntity
import com.example.data.entities.NoteWithCategoryColor
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class NoteLocalDataSource @Inject constructor(
    private val noteDao: NoteDao,
    private val categoryDao: CategoryDao
) {
    suspend fun deleteNote(note: NoteEntity) {
        noteDao.deleteNote(note)
    }

    suspend fun updateNote(note: NoteEntity) {
        noteDao.updateNote(note)
    }

    suspend fun insertNote(note: NoteEntity) {
        noteDao.insertNote(note)
    }

    suspend fun insertCategory(category: CategoryEntity) {
        categoryDao.insertCategory(category)
    }

    suspend fun deleteCategory(category: CategoryEntity) {
        categoryDao.deleteCategory(category)
    }

    suspend fun updateCategory(category: CategoryEntity) {
        categoryDao.updateCategory(category)
    }

    fun findNotesByCategory(category: CategoryEntity) =
        noteDao.findNotesByCategory(categoryId = category.categoryId)

    fun fetchCategories(): Flow<List<CategoryWithNumOfNote>> = categoryDao.fetchAllCategories()
}