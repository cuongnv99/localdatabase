package com.example.data.repository

import com.example.data.datasource.NoteLocalDataSource
import com.example.data.entities.CategoryWithNumOfNote
import com.example.data.entities.NoteWithCategoryColor
import com.example.data.mapper.toDomain
import com.example.data.mapper.toEntity
import com.example.data.mapper.toNoteDomain
import com.example.domain.dto.Category
import com.example.domain.dto.Note
import com.example.domain.repository.NoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class NoteRepositoryImpl @Inject constructor(
    private val noteDataSource: NoteLocalDataSource
) : NoteRepository {

    override fun getNotesByCategory(category: Category): Flow<List<Note>> =
        noteDataSource
            .findNotesByCategory(category = category.toEntity())
            .map(::fromNotesEntity)


    override fun getCategories(): Flow<List<Category>> =
        noteDataSource.fetchCategories()
            .map(::fromCategoriesEntity)

    override suspend fun addNote(note: Note) {
        noteDataSource.insertNote(note = note.toEntity())
    }

    override suspend fun deleteNote(note: Note) =
        noteDataSource.deleteNote(note = note.toEntity())

    override suspend fun updateNote(note: Note) {
        noteDataSource.updateNote(note = note.toEntity())
    }

    override suspend fun addCategory(category: Category) {
        noteDataSource.insertCategory(category = category.toEntity())
    }

    private fun fromNotesEntity(notes: List<NoteWithCategoryColor>): List<Note> =
        notes.map(NoteWithCategoryColor::toNoteDomain)

    private fun fromCategoriesEntity(categories: List<CategoryWithNumOfNote>): List<Category> =
        categories.map { (category, numberOfNote) ->
            category.toDomain(numberOfNote)
        }
}