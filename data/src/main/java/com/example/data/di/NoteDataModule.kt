package com.example.data.di

import android.content.Context
import com.example.data.database.SampleAppDatabase
import com.example.data.repository.NoteRepositoryImpl
import com.example.domain.repository.NoteRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NoteDataModule {

    @Binds
    abstract fun bindNoteRepository(noteRepositoryImpl: NoteRepositoryImpl): NoteRepository

    companion object {
        private const val NOTE_DATABASE_NAME = "note_database"

        @Provides
        fun provideCategoryDao(appDatabase: SampleAppDatabase) = appDatabase.categoryDao()

        @Provides
        fun provideNoteDao(appDatabase: SampleAppDatabase) = appDatabase.noteDao()

        @Provides
        @Singleton
        fun provideNoteAppDatabase(@ApplicationContext context: Context): SampleAppDatabase =
            SampleAppDatabase.getDatabase(context)
    }
}