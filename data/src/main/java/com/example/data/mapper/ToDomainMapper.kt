package com.example.data.mapper

import com.example.data.entities.CategoryEntity
import com.example.data.entities.NoteEntity
import com.example.data.entities.NoteWithCategoryColor
import com.example.domain.dto.Category
import com.example.domain.dto.Note

fun NoteWithCategoryColor.toNoteDomain() = Note(
    id = note.noteId,
    title = note.title,
    content = note.content,
    createdAt = note.createdAt,
    color = color
)

fun CategoryEntity.toDomain(numberOfNote: Int) =
    Category(id = categoryId, name = name, color = color, numberOfNote = numberOfNote)