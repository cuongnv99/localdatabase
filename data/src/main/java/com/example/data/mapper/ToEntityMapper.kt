package com.example.data.mapper

import com.example.data.entities.CategoryEntity
import com.example.data.entities.NoteEntity
import com.example.domain.dto.Category
import com.example.domain.dto.Note

fun Note.toEntity() = NoteEntity(noteId = id, title = title, content = content, createdAt = createdAt, categoryId = categoryId)

fun Category.toEntity() = CategoryEntity(categoryId = id, name = name, color = color)