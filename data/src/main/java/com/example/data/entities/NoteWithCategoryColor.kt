package com.example.data.entities

import androidx.room.Embedded

data class NoteWithCategoryColor(
    @Embedded val note: NoteEntity,
    val color: String
)