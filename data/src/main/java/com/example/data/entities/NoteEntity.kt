package com.example.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.data.CommonConstants
import java.time.LocalDateTime

@Entity(tableName = CommonConstants.NOTE_TABLE_NAME)
data class NoteEntity(
    @PrimaryKey(autoGenerate = true) val noteId: Long,
    val categoryId: Long? = null,
    val title: String = "",
    val content: String = "",
    val createdAt: LocalDateTime,
    @ColumnInfo(name = "categoryPriority", defaultValue = "0")
    val priority: Int = 0,
)