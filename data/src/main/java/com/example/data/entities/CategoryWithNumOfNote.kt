package com.example.data.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded

data class CategoryWithNumOfNote(
    @Embedded val category: CategoryEntity,
    @ColumnInfo(name = "number_of_notes")
    val numberOfNote: Int = 0
)