package com.example.data.entities

import androidx.room.*
import com.example.data.CommonConstants.CATEGORY_TABLE_NAME

@Entity(tableName = CATEGORY_TABLE_NAME)
data class CategoryEntity(
    @PrimaryKey(autoGenerate = true) val categoryId: Long,
    val name: String = "",
    val color: String = "",
    @ColumnInfo(defaultValue = "0")
    val level: String = "0",
    @ColumnInfo(defaultValue = "0")
    val authorNew: String = "0")
