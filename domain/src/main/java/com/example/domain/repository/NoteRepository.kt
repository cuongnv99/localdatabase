package com.example.domain.repository

import com.example.domain.dto.Category
import com.example.domain.dto.Note
import kotlinx.coroutines.flow.Flow

interface NoteRepository {

    fun getNotesByCategory(category: Category): Flow<List<Note>>

    fun getCategories(): Flow<List<Category>>

    suspend fun addNote(note: Note)

    suspend fun deleteNote(note: Note)

    suspend fun updateNote(note: Note)
    suspend fun addCategory(category: Category)
}