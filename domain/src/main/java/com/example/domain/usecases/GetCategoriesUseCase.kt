package com.example.domain.usecases

import com.example.domain.core.BaseUseCase
import com.example.domain.core.IoDispatcher
import com.example.domain.dto.Category
import com.example.domain.repository.NoteRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Get category
 */
class GetCategoriesUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val noteRepository: NoteRepository
) : BaseUseCase<Unit, Flow<List<Category>>>(dispatcher) {
    override suspend fun execute(parameter: Unit): Flow<List<Category>> {
        return noteRepository.getCategories()
    }
}