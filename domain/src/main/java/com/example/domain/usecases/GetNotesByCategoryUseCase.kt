package com.example.domain.usecases

import com.example.domain.core.BaseUseCase
import com.example.domain.core.IoDispatcher
import com.example.domain.dto.Category
import com.example.domain.dto.Note
import com.example.domain.repository.NoteRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetNotesByCategoryUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val noteRepository: NoteRepository
) : BaseUseCase<Category, Flow<List<Note>>>(dispatcher) {
    override suspend fun execute(parameter: Category): Flow<List<Note>> {
        return noteRepository.getNotesByCategory(category = parameter)
    }
}