package com.example.domain.usecases

import com.example.domain.core.BaseUseCase
import com.example.domain.core.IoDispatcher
import com.example.domain.dto.Note
import com.example.domain.repository.NoteRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class DeleteNoteUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val noteRepository: NoteRepository
) : BaseUseCase<Note, Unit>(dispatcher) {
    override suspend fun execute(parameter: Note) {
        noteRepository.deleteNote(note = parameter)
    }
}