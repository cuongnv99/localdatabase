package com.example.domain.usecases

import com.example.domain.core.BaseUseCase
import com.example.domain.core.IoDispatcher
import com.example.domain.dto.Category
import com.example.domain.repository.NoteRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class AddNewCategoryUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val noteRepository: NoteRepository
) : BaseUseCase<Category, Unit>(dispatcher) {
    override suspend fun execute(parameter: Category) {
        noteRepository.addCategory(category = parameter)
    }
}