package com.example.domain.dto

import java.time.LocalDateTime
import java.io.Serializable

data class Note(
    val id: Long = 0,
    val categoryId: Long = 0,
    val title: String = "",
    val content: String = "",
    val createdAt: LocalDateTime = LocalDateTime.now(),
    val color: String = NOTE_DEFAULT_COLOR
) : Serializable {
    companion object {
        private const val NOTE_DEFAULT_COLOR = "#fffff"
    }
}