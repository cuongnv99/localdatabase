package com.example.domain.dto

data class Category(
    val id: Long = 0,
    val name: String = "",
    val color: String = "",
    val numberOfNote: Int = 0
) {
    override fun toString(): String = name
}