package com.example.domain.core

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.logging.LogManager

abstract class BaseUseCase<in P, out R>(private val dispatcher: CoroutineDispatcher) {
    suspend operator fun invoke(parameter: P): Result<R> =
        withContext(dispatcher) {
            runCatching {
                execute(parameter)
            }.onFailure {
                it.printStackTrace()
            }
        }

    protected abstract suspend fun execute(parameter: P): R
}