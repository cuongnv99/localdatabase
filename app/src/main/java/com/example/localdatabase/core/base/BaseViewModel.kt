package com.example.localdatabase.core.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

abstract class BaseViewModel<S, E> : ViewModel() {
    private val eventChannel = Channel<E>()
    abstract fun initState(): S
    private val uiState by lazy { MutableStateFlow(initState()) }
    val currentState get() = uiState.value
    val event = eventChannel.receiveAsFlow()

    protected fun sendEvent(event: E) {
        viewModelScope.launch {
            eventChannel.send(event)
        }
    }

    protected fun dispatchState(state: S) {
        viewModelScope.launch {
            uiState.emit(state)
        }
    }

    fun <F> CoroutineScope.observeUiState(
        selector: (S) -> F,
        observer: (uiState: F) -> Unit
    ) {
        uiState.map(selector).distinctUntilChanged().onEach(observer).launchIn(this)
    }

    fun CoroutineScope.observeEvent(observer: (E)-> Unit){
        event.onEach(observer).launchIn(this)
    }
}