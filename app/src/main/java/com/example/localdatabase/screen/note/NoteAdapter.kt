package com.example.localdatabase.screen.note

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.dto.Note
import com.example.localdatabase.databinding.ItemNoteBinding
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class NoteAdapter(private val onNoteDeleted: (id: Long) -> Unit) :
    ListAdapter<Note, NoteAdapter.NoteViewHolder>(DIFF) {

    class NoteViewHolder(
        private val binding: ItemNoteBinding,
        private val onDeleteClicked: (id: Long) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        private var noteId: Long = 0

        init {
            binding.btnDelete.setOnClickListener {
                onDeleteClicked(noteId)
            }
        }

        fun bind(note: Note) = with(binding) {
            noteId = note.id
            tvDate.text = note.createdAt.format(DateTimeFormatter.ofPattern(DATE_PATTERN))
            tvTitle.text = note.title
            if(note.color.isEmpty()) return@with
            color.setBackgroundColor(Color.parseColor(note.color))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder =
        NoteViewHolder(
            binding = ItemNoteBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onDeleteClicked = onNoteDeleted
        )

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    companion object {
        private const val DATE_PATTERN = "dd\nMMM"

        private val DIFF = object : DiffUtil.ItemCallback<Note>() {
            override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean =
                oldItem == newItem
        }
    }
}