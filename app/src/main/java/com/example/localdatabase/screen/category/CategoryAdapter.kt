package com.example.localdatabase.screen.category

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.dto.Category
import com.example.localdatabase.databinding.ItemCategoryBinding
import javax.inject.Inject

class CategoryAdapter(private val categorySelected: (category: Category) -> Unit) :
    ListAdapter<Category, CategoryAdapter.CategoryViewHolder>(DIFF) {

    class CategoryViewHolder(
        private val binding: ItemCategoryBinding,
        private val itemSelected: (category: Category) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var category: Category

        init {
            binding.root.setOnClickListener {
                if (::category.isLateinit) {
                    itemSelected(category)
                }
            }
        }

        @SuppressLint("Range")
        fun bind(category: Category) = with(binding) {
            this@CategoryViewHolder.category = category
            tvCategoryName.text = category.name
            tvNumberOfNote.text = category.numberOfNote.toString()
            if (category.color.isEmpty()) return@with
            root.setCardBackgroundColor(Color.parseColor(category.color))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
        CategoryViewHolder(
            binding = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            itemSelected = categorySelected
        )

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DIFF = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem == newItem
        }
    }
}