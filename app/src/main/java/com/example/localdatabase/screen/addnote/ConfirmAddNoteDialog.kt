package com.example.localdatabase.screen.addnote

import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.domain.dto.Category
import com.example.domain.dto.Note
import com.example.localdatabase.R
import com.example.localdatabase.core.base.BaseDialogFragment
import com.example.localdatabase.databinding.DialogConfirmAddNoteBinding
import com.example.localdatabase.screen.addnote.AddNoteFragment.Companion.ADD_NOTE_REQUEST_KEY
import com.example.localdatabase.screen.main.MainEvent
import com.example.localdatabase.screen.main.MainViewState
import com.example.localdatabase.screen.main.NoteViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ConfirmAddNoteDialog : BaseDialogFragment<DialogConfirmAddNoteBinding>() {
    override fun getViewBinding(): DialogConfirmAddNoteBinding =
        DialogConfirmAddNoteBinding.inflate(layoutInflater)

    private val noteViewModel: NoteViewModel by activityViewModels()

    override fun initActions() {
        super.initActions()
        binding.btnCancel.setOnClickListener {
            dismiss()
        }

        binding.btnDone.setOnClickListener {
            performAddNote()
        }
    }

    private fun performAddNote() {
        val note = (arguments?.get(NOTE_NEED_ADD) as? Note) ?: return
        val categorySelected = binding.snCategory.selectedItem as Category
        noteViewModel.addNote(note = note.copy(categoryId = categorySelected.id))
        noteViewModel.getNotesByCategory(categorySelected)
        parentFragment?.setFragmentResult(ADD_NOTE_REQUEST_KEY, bundleOf())
        dismiss()
    }

    override fun initObservers(): Unit = with(noteViewModel) {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeUiState(
                    selector = MainViewState::categories,
                    observer = ::displayCategoriesSpinner
                )

                observeEvent { event ->
                    when (event) {
                        MainEvent.PerformActionSuccess, MainEvent.PerformActionFailed -> {
                            dismiss()
                        }
                        else -> {
                            // Nothing
                        }
                    }
                }
            }
        }
    }

    private fun displayCategoriesSpinner(categories: List<Category>) {
        binding.snCategory.adapter = ArrayAdapter(
            requireContext(),
            R.layout.item_spinner,
            categories
        )
    }

    companion object {
        private const val NOTE_NEED_ADD = "NOTE_NEED_ADD"

        fun newInstance(note: Note): ConfirmAddNoteDialog {
            return ConfirmAddNoteDialog().apply {
                arguments = bundleOf(
                    NOTE_NEED_ADD to note
                )
            }
        }
    }
}