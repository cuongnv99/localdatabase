package com.example.localdatabase.screen.addnote

import androidx.fragment.app.setFragmentResultListener
import com.example.domain.dto.Note
import com.example.localdatabase.core.base.BaseFragment
import com.example.localdatabase.databinding.FragmentAddNoteBinding
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDateTime

@AndroidEntryPoint
class AddNoteFragment : BaseFragment<FragmentAddNoteBinding>() {
    override fun getViewBinding(): FragmentAddNoteBinding =
        FragmentAddNoteBinding.inflate(layoutInflater)

    override fun initAction() = with(binding) {
        btnBack.setOnClickListener { onBackPressed() }
        btnDone.setOnClickListener {
            ConfirmAddNoteDialog.newInstance(
                Note(
                    title = edtTitle.text.toString(),
                    content = edtNote.text.toString(),
                    createdAt = LocalDateTime.now(),
                )
            ).show(childFragmentManager, null)
        }

        setFragmentResultListener(ADD_NOTE_REQUEST_KEY) { _, _ ->
            onBackPressed()
        }
    }

    companion object {
        const val ADD_NOTE_REQUEST_KEY = "ADD_NOTE_REQUEST_KEY"
    }
}