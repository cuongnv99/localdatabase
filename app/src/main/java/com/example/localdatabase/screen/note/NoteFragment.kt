package com.example.localdatabase.screen.note

import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.domain.dto.Note
import com.example.localdatabase.core.base.BaseFragment
import com.example.localdatabase.databinding.FragmentNoteBinding
import com.example.localdatabase.screen.main.MainViewState
import com.example.localdatabase.screen.main.NoteViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NoteFragment : BaseFragment<FragmentNoteBinding>() {
    override fun getViewBinding(): FragmentNoteBinding =
        FragmentNoteBinding.inflate(layoutInflater)

    private val viewModel: NoteViewModel by activityViewModels()

    private lateinit var noteAdapter: NoteAdapter

    override fun initView() {
        setUpNoteRecyclerView()
    }

    override fun initObserver(): Unit = with(viewModel) {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeUiState(
                    selector = MainViewState::notes,
                    observer = ::displayNotes
                )

                observeUiState(
                    selector = MainViewState::categorySelected,
                    observer = { category ->
                        binding.tvCategoryName.text = category?.name ?: ""
                    }
                )
            }
        }
    }

    private fun displayNotes(notes: List<Note>) = with(binding) {
        // Set visible for empty view and recycler view
        grEmptyNote.isVisible = notes.isEmpty().also { isNotesEmpty ->
            rvNote.isVisible = !isNotesEmpty
        }

        noteAdapter.submitList(notes)
    }

    private fun setUpNoteRecyclerView() = with(binding) {
        noteAdapter = NoteAdapter(onNoteDeleted = { noteId ->
            viewModel.deleteNote(noteId)
        })
        rvNote.adapter = noteAdapter
    }
}