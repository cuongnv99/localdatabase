package com.example.localdatabase.screen.main

import androidx.lifecycle.viewModelScope
import com.example.domain.dto.Category
import com.example.domain.dto.Note
import com.example.domain.usecases.*
import com.example.localdatabase.core.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NoteViewModel @Inject constructor(
    private val addNewNoteUseCase: AddNewNoteUseCase,
    private val addCategoryUse: AddNewCategoryUseCase,
    private val getCategoriesUseCase: GetCategoriesUseCase,
    private val getNotesByCategoryUseCase: GetNotesByCategoryUseCase,
    private val deleteNoteUseCase: DeleteNoteUseCase
) : BaseViewModel<MainViewState, MainEvent>() {
    private var notesJob: Job? = null

    override fun initState(): MainViewState = MainViewState()

    fun addNote(note: Note) {
        viewModelScope.launch {
            addNewNoteUseCase(note).onSuccess {
                sendEvent(MainEvent.PerformActionSuccess)
            }
        }
    }

    fun addCategory(category: Category) {
        viewModelScope.launch {
            addCategoryUse(category).onSuccess {
                sendEvent(MainEvent.PerformActionSuccess)
            }
        }
    }

    fun getCategories() {
        viewModelScope.launch {
            getCategoriesUseCase(Unit).onSuccess { categoriesFlow ->
                categoriesFlow.collect { categories ->
                    dispatchState(currentState.copy(categories = categories))
                }
            }
        }
    }

    fun getNotesByCategory(category: Category) {
        dispatchState(currentState.copy(categorySelected = category))
        notesJob?.cancel()
        notesJob = viewModelScope.launch {
            getNotesByCategoryUseCase(Category(category.id))
                .onSuccess { notesFlow ->
                    notesFlow.collect { notes ->
                        dispatchState(currentState.copy(notes = notes))
                    }
                }
        }
    }

    fun selectNoteScreen() {
        sendEvent(MainEvent.NavigateNoteScreen)
    }

    fun deleteNote(noteId: Long) {
        viewModelScope.launch {
            deleteNoteUseCase(Note(noteId))
        }
    }
}