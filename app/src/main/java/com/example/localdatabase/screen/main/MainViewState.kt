package com.example.localdatabase.screen.main

import com.example.domain.dto.Category
import com.example.domain.dto.Note

data class MainViewState(
    val loading: Boolean = false,
    val categorySelected: Category? = null,
    val categories: List<Category> = listOf(),
    val notes: List<Note> = listOf()
)