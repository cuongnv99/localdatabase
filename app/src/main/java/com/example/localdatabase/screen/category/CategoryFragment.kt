package com.example.localdatabase.screen.category

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.localdatabase.core.base.BaseFragment
import com.example.localdatabase.databinding.FragmentCategoryBinding
import com.example.localdatabase.screen.main.MainViewState
import com.example.localdatabase.screen.main.NoteViewModel
import com.example.localdatabase.screen.category.CategoryAdapter
import kotlinx.coroutines.launch

class CategoryFragment : BaseFragment<FragmentCategoryBinding>() {
    override fun getViewBinding(): FragmentCategoryBinding =
        FragmentCategoryBinding.inflate(layoutInflater)

    private val viewModel: NoteViewModel by activityViewModels()


    private lateinit var categoryAdapter: CategoryAdapter

    override fun initData() {
        super.initData()
        viewModel.getCategories()
    }

    override fun initView() {
        setupCategoryRecyclerView()
    }

    override fun initObserver(): Unit = with(viewModel) {
        super.initObserver()
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeUiState(
                    selector = MainViewState::categories,
                    observer = categoryAdapter::submitList
                )
            }
        }
    }

    private fun setupCategoryRecyclerView() = with(binding.rvCategory) {
        categoryAdapter = CategoryAdapter{ category ->
            viewModel.getNotesByCategory(category)
            viewModel.selectNoteScreen()
        }
        layoutManager =
            GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)
        adapter = categoryAdapter
    }
}