package com.example.localdatabase.screen.addcategory

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.domain.dto.Category
import com.example.localdatabase.core.base.BaseFragment
import com.example.localdatabase.databinding.FragmentAddCategoryBinding
import com.example.localdatabase.screen.main.NoteViewModel

class AddCategoryFragment : BaseFragment<FragmentAddCategoryBinding>() {
    override fun getViewBinding(): FragmentAddCategoryBinding =
        FragmentAddCategoryBinding.inflate(layoutInflater)

    private val noteViewModel: NoteViewModel by activityViewModels()

    override fun initView() {
        binding.rvColorPicker.init()
    }

    override fun initAction() = with(binding) {
        btnBack.setOnClickListener {
            onBackPressed()
        }
        btnDone.setOnClickListener {
            noteViewModel.addCategory(
                category = Category(
                    name = edtCategoryName.text.toString(),
                    color = binding.rvColorPicker.colorSelected
                )
            )
            onBackPressed()
        }
    }
}