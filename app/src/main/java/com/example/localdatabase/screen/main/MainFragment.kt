package com.example.localdatabase.screen.main

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.localdatabase.R
import com.example.localdatabase.core.base.BaseFragment
import com.example.localdatabase.databinding.FragmentMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>() {

    override fun getViewBinding(): FragmentMainBinding = FragmentMainBinding.inflate(layoutInflater)
    private val viewModel: NoteViewModel by activityViewModels()
    private val currentScreenSelected: Screen
        get() = if (binding.navBottom.selectedItemId == R.id.menuCategory) {
            Screen.CATEGORY
        } else {
            Screen.NOTE
        }

    private lateinit var mainPagerAdapter: MainPagerAdapter

    override fun initView() {
        setUpMainViewPager()
    }

    override fun initAction() {
        initBottomNavigationActions()
        initMainViewPagerActions()
        setActionOfButtons()
    }

    private fun setActionOfButtons() = with(binding) {
        btnAddNote.setOnClickListener { performActionAdd() }
    }

    override fun initObserver(): Unit = with(viewModel) {
        super.initObserver()
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeEvent { mainEvent ->
                    if (mainEvent is MainEvent.NavigateNoteScreen) {
                        binding.navBottom.selectedItemId = MenuScreen.values().first().menuId
                        binding.vpMain.currentItem = 0
                    }
                }
            }
        }
    }

    private fun initMainViewPagerActions() {
        binding.vpMain.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                binding.navBottom.selectedItemId = MenuScreen.values()[position].menuId
            }
        })
    }

    private fun initBottomNavigationActions() = with(binding) {
        navBottom.setOnItemSelectedListener { menuItem ->
            vpMain.currentItem =
                MenuScreen.values().firstOrNull { it.menuId == menuItem.itemId }?.ordinal ?: 0
            true
        }
    }

    private fun setUpMainViewPager() {
        mainPagerAdapter = MainPagerAdapter(this)
        binding.vpMain.adapter = mainPagerAdapter
    }

    enum class MenuScreen(val menuId: Int) {
        NOTE_MENU(R.id.menuNote), CATEGORY_MENU(R.id.menuCategory)
    }

    private fun performActionAdd() {
        val action = when (currentScreenSelected) {
            Screen.NOTE -> {
                MainFragmentDirections.actionMainFragmentToAddNoteFragment()
            }

            Screen.CATEGORY -> {
                MainFragmentDirections.actionMainFragmentToAddCategoryFragment()
            }
        }
        findNavController().navigate(action)
    }

    companion object {
        enum class Screen {
            NOTE, CATEGORY
        }
    }
}