package com.example.localdatabase.screen.main

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.localdatabase.screen.category.CategoryFragment
import com.example.localdatabase.screen.note.NoteFragment

class MainPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment =
        if (position == 0) {
            NoteFragment()
        } else {
            CategoryFragment()
        }
}