package com.example.localdatabase.screen.main

// TODO only for demo
sealed interface MainEvent {
    object PerformActionSuccess : MainEvent

    object PerformActionFailed : MainEvent

    object NavigateNoteScreen: MainEvent
}