package com.example.localdatabase.common.colorpicker

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.localdatabase.R

class ColorPicker(context: Context, attributeSet: AttributeSet) :
    RecyclerView(context, attributeSet) {
    private lateinit var colorPickerAdapter: ColorPickerAdapter

    val colorSelected: String get() = colorPickerAdapter.colorSelected

    init {
        background = AppCompatResources.getDrawable(context, R.drawable.background_color_picker)
        layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
    }

    fun init(colors: List<String> = colorsDefault) {
        colorPickerAdapter = ColorPickerAdapter().apply {
            submitList(colors.map { ColorItem(value = it) })
        }
        adapter = colorPickerAdapter
    }

    private val colorsDefault =
        listOf("#ef4444", "#f97316", "#facc15", "#4ade80", "#2dd4bf", "#3b82f6", "#6467f2")
}