package com.example.localdatabase.common.colorpicker

import java.util.UUID

data class ColorItem(
    val id: String = UUID.randomUUID().toString(),
    val value: String,
    val checked: Boolean = false
)