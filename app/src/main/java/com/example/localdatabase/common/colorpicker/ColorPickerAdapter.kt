package com.example.localdatabase.common.colorpicker

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.localdatabase.core.views.px

class ColorPickerAdapter :
    ListAdapter<ColorItem, ColorPickerAdapter.ColorViewHolder>(diff) {

    var colorSelected: String = ""
        private set

    class ColorViewHolder(val colorCircle: ColorCircle) :
        RecyclerView.ViewHolder(colorCircle) {
        fun bind(colorItem: ColorItem) {
            colorCircle.colorSelected = colorItem.checked
            colorCircle.mainColor = colorItem.value
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder =
        ColorViewHolder(colorCircle = ColorCircle(ITEM_SIZE, parent.context))

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val colorItem = getItem(position)
        holder.bind(colorItem = colorItem)
        holder.colorCircle.setOnClickListener {
            colorSelected = colorItem.value
            submitList(currentList.map { it.copy(checked = it.id == colorItem.id) })
        }
    }

    companion object {
        private val ITEM_SIZE = 45.px

        private val diff = object : ItemCallback<ColorItem>() {
            override fun areItemsTheSame(oldItem: ColorItem, newItem: ColorItem): Boolean =
                oldItem.value == newItem.value

            override fun areContentsTheSame(oldItem: ColorItem, newItem: ColorItem): Boolean =
                oldItem == newItem
        }
    }
}