package com.example.localdatabase.common.colorpicker

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.example.localdatabase.R
import com.example.localdatabase.core.views.px

class ColorCircle : View {
    private var size: Int = 0
    var mainColor: String = "#FFFFFF"
        set(value) {
            field = value
            invalidate()
        }

    var colorSelected: Boolean = false
        set(value) {
            field = value
            invalidate()
        }

    constructor(size: Int, context: Context) : super(context) {
        this.size = size
    }

    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    constructor(context: Context, attr: AttributeSet, defStyle: Int) : super(
        context,
        attr,
        defStyle
    )

    private var paint = Paint().apply {
        isAntiAlias = true
    }
    private var borderSelectedColor = context.getColor(R.color.gray_600)
    private var innerSelectedColor = Color.WHITE
    private val radius get() = size / 2f
    private val innerRadius get() = radius - INNER_RADIUS_OFFSET


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas) {
        drawChecked(canvas)
        drawMainColor(canvas)
    }

    private fun drawMainColor(canvas: Canvas) {
        paint.apply {
            color = Color.parseColor(mainColor)
            style = Paint.Style.FILL
        }
        canvas.drawCircle(radius, radius, innerRadius - SPACE_ITEMS, paint)
    }

    private fun drawChecked(canvas: Canvas) {
        if (!colorSelected) return
        paint.apply {
            color = innerSelectedColor
            style = Paint.Style.STROKE
            strokeWidth = BORDER_WIDTH_SELECTED
            canvas.drawCircle(radius, radius, radius - SPACE_ITEMS, this)
            color = borderSelectedColor
            canvas.drawCircle(
                radius,
                radius,
                radius - SPACE_ITEMS - BORDER_WIDTH_SELECTED / 2,
                this
            )
        }
    }

    companion object {
        private val BORDER_WIDTH_SELECTED = 3f.px
        private val INNER_RADIUS_OFFSET = 5.px
        private val SPACE_ITEMS = 2.px
    }
}