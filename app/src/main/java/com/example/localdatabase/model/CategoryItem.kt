package com.example.localdatabase.model

import com.example.domain.dto.Category

data class CategoryItem(
    val id: Long,
    val name: String,
    val color: String,
    val numberOfNote: Int = 0
)

fun Category.toDisplayModel(numberOfNote: Int): CategoryItem =
    CategoryItem(
        id = id,
        name = name,
        color = color,
        numberOfNote = numberOfNote
    )