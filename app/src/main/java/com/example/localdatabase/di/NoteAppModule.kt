package com.example.localdatabase.di

import com.example.domain.core.DefaultDispatcher
import com.example.domain.core.IoDispatcher
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers

@Module
@InstallIn(SingletonComponent::class)
abstract class NoteViewModelModule {
    companion object {
        @Provides
        @IoDispatcher
        fun provideIoDispatcher() = Dispatchers.IO

        @Provides
        @DefaultDispatcher
        fun provideDefaultDispatcher() = Dispatchers.Default
    }
}